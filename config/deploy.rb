require "rvm/capistrano"
require 'bundler/capistrano'
set :application, "162.243.61.51"
set :repository, "git@bitbucket.org:frankystonlins/xmltojson.git"
set :user, "frankyston"
set :use_sudo, false
set :deploy_to, "/home/frankyston/apps/xmltojson"
set :scm, :git

server application, :app, :web, :db, :primary => true
after "deploy:create_symlink", "deploy:custom_symlink"

namespace :deploy do
  task :start do ; end
  task :stop do ; end
#task to create upload image folder
  task :custom_symlink, :roles=> :app do 
  	run "ln -nfs #{shared_path}/database.yml #{release_path}/config/database.yml"
  end
  
  task :minha_tarefa, :roles=> :app do 
  	puts "minha tarefa"
  end
  
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path, 'tmp','restart.txt')}"
  end
end

namespace :rvm do
  task :trust_rvmrc do
    run "rvm rvmrc trust #{release_path}"
  end
end