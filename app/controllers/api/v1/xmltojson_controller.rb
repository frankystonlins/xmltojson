class Api::V1::XmltojsonController < ApplicationController

	def create
		render :json=> Hash.from_xml(params[:xml]).to_json
	end

end
