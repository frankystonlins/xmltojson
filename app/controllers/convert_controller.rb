#require 'premailer'
class ConvertController < ApplicationController
  layout "layconvert"
  def index
	  return if params[:converts].blank?

	  #@attachment = Convert.new
	  require 'json'
	  require 'digest'

	  @file = params[:converts].tempfile.path
	  @file_destino = Digest::MD5.hexdigest(params[:converts].original_filename.to_s) + ".json"
	  
	  File.open("public/xml/"+ @file_destino, 'w') {|f| f.write(Hash.from_xml(File.open(@file)).to_json)}

	  session[:file] = @file_destino
	  render :template => "convert/down"
  end


  def down
  	require 'open-uri'
	url = 'public/xml/' + session[:file]
	data = open(url).read
	send_data data, :disposition => 'attachment', :filename=> session[:file]
  end

  def api
  	
  end

  def cssinline

  end

  def pdf 
    
  end

  def download_pdf 
    file = Tempfile.new('foo')
    `wkhtmltopdf #{params[:url]} #{file.path}`
    send_file file.path, :x_sendfile => true, :type => 'pdf', :filename=>"converted.pdf"
  end

  def download_cssinline
  	#'http://eitamah.coredigital.com.br/public_html/cupons/para-impressao/060812469669AE02A86'
  	premailer = Premailer.new(params[:url], :warn_level => Premailer::Warnings::SAFE)
  	file = Tempfile.new('foo')
  	file.write premailer.to_inline_css
  	file.rewind
  	file.read      # => "hello world"
  	file.close

  	send_file file.path, :x_sendfile => true, :type => 'html', :filename=>"converted.html"
  end
end
